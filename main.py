SAMPLING_RATE = 16000

import torch
import librosa

from pprint import pprint

from flask import Flask, jsonify
from flask import request
from flask import render_template
import os

torch.set_num_threads(1)


model, utils = torch.hub.load(
    repo_or_dir="~/hackManthan/models/",
    model="silero_vad_extended",
    force_reload=True,
)

(get_speech_timestamps, save_audio, read_audio, VADIterator, collect_chunks) = utils


def pred(file):
    y, sr = librosa.load(file, sr=SAMPLING_RATE, mono=True)
    # get speech timestamps from full audio file
    speech_timestamps = get_speech_timestamps(y, model, sampling_rate=SAMPLING_RATE)
    return speech_timestamps


# creates a Flask application, named app
app = Flask(__name__)
app._static_folder = "templates/static"

# a route where we will display a welcome message via an HTML template
@app.route("/")
def hello():
    message = "Hello, World"
    return render_template("index.html", message=message)


@app.route("/upload", methods=["POST", "GET"])
def upload():
    if request.method == "POST":
        f = request.files["audio_data"]
        with open("audio.wav", "wb") as audio:
            f.save(audio)
        print("file uploaded successfully")
        message = "Upload Successful"
        p = pred("audio.wav")
        return jsonify(p)


# run the application
if __name__ == "__main__":
    app.secret_key = os.urandom(24)
    app.run(debug=True)
