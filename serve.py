# serve.py

from flask import Flask
from flask import request
from flask import render_template
import os

# creates a Flask application, named app
app = Flask(__name__)
app._static_folder = "templates/static"

# a route where we will display a welcome message via an HTML template
@app.route("/")
def hello():
    message = "Hello, World"
    return render_template("index.html", message=message)


@app.route("/upload", methods=["POST", "GET"])
def upload():
    if request.method == "POST":
        f = request.files["audio_data"]
        with open("audio.wav", "wb") as audio:
            f.save(audio)
        print("file uploaded successfully")
        message = "Upload Successful"
        return render_template("index.html", message=message)


# run the application
if __name__ == "__main__":
    app.run(debug=True)
